package com.example.masrobot.myfootballclub.repository

import java.net.URL

class ApiRepository {
    fun doRequest(url: String): String = URL(url).readText()
}