package com.example.masrobot.myfootballclub.teamdetail

import com.example.masrobot.myfootballclub.model.TeamResponse
import com.example.masrobot.myfootballclub.repository.ApiRepository
import com.example.masrobot.myfootballclub.repository.TheSportDBApi
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamDetailPresenter(private val view: TeamDetailView,
                          private val apiRepository: ApiRepository,
                          private val gson: Gson) {
    fun getTeamDetail(teamId: String) {
        view.showLoading()
        async(UI) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeamDetail(teamId)),
                        TeamResponse::class.java)
            }
            view.showTeamDetail(data.await().teams)
            view.hideLoading()
        }
    }
}