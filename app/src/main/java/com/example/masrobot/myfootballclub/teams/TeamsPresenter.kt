package com.example.masrobot.myfootballclub.teams

import android.util.Log
import com.example.masrobot.myfootballclub.model.TeamResponse
import com.example.masrobot.myfootballclub.repository.ApiRepository
import com.example.masrobot.myfootballclub.repository.TheSportDBApi
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg


class TeamsPresenter(private val view: TeamsView,
                     private val apiRepository: ApiRepository,
                     private val gson: Gson) {
    fun getTeamList(league: String?) {
        view.showLoading()

        async(UI) {
            val data = bg {
                gson.fromJson(apiRepository.doRequest(TheSportDBApi.getTeams(league)),
                        TeamResponse::class.java)
            }
            view.showTeamList(data.await().teams)
            Log.d("DataTeam", data.await().teams.toString())
            view.hideLoading()
        }
    }
}