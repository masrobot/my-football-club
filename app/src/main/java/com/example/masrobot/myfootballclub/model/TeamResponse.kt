package com.example.masrobot.myfootballclub.model

data class TeamResponse(val teams: List<Team>)