package com.example.masrobot.myfootballclub.teams

import com.example.masrobot.myfootballclub.model.Team

interface TeamsView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}