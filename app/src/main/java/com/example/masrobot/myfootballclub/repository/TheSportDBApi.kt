package com.example.masrobot.myfootballclub.repository

import com.example.masrobot.myfootballclub.BuildConfig

object TheSportDBApi {
    fun getTeams(league: String?): String = "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/search_all_teams.php?l=$league"
    fun getTeamDetail(teamId: String?) : String = "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/lookupteam.php?id=$teamId"
}