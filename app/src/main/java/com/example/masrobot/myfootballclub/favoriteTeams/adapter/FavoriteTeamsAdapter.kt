package com.example.masrobot.myfootballclub.favoriteTeams.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.masrobot.myfootballclub.R
import com.example.masrobot.myfootballclub.db.Favorite
import com.example.masrobot.myfootballclub.layout.TeamUI
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick

class FavoriteTeamsAdapter(private val favorite: List<Favorite>,
                           private val listener: (Favorite) -> Unit) : RecyclerView.Adapter<FavoriteViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder =
            FavoriteViewHolder(TeamUI().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bindItem(favorite[position], listener)
    }
}

class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val teamBadge = view.findViewById<ImageView>(R.id.team_badge)
    private val teamName = view.findViewById<TextView>(R.id.team_name)

    fun bindItem(favorite: Favorite, listener: (Favorite) -> Unit) {
        Picasso.with(itemView.context).load(favorite.teamBadge).into(teamBadge)
        teamName.text = favorite.teamName
        itemView.onClick {
            listener(favorite)
        }
    }
}
