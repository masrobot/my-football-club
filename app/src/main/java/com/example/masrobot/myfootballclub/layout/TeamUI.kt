package com.example.masrobot.myfootballclub.layout

import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.masrobot.myfootballclub.R
import org.jetbrains.anko.*

class TeamUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        linearLayout {
            // Layout Params
            lparams(matchParent, wrapContent)
            padding = dip(16)
            orientation = LinearLayout.HORIZONTAL
            id = R.id.container

            // Image view
            imageView {
                id = R.id.team_badge
            }.lparams(dip(50), dip(50))

            // Text view
            textView {
                id = R.id.team_name
                textSize = 16f
            }.lparams {
                margin = dip(15)
            }
        }
    }
}