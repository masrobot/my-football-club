package com.example.masrobot.myfootballclub.teams.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.masrobot.myfootballclub.R
import com.example.masrobot.myfootballclub.layout.TeamUI
import com.example.masrobot.myfootballclub.model.Team
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick

class TeamsAdapter (private val teams: List<Team>,
                    private val listener: (Team) -> Unit) : RecyclerView.Adapter<TeamViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder =
            TeamViewHolder(TeamUI().createView(AnkoContext.create(parent.context, parent)))

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(teams[position], listener)
    }

}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val teamBadge = view.findViewById<ImageView>(R.id.team_badge)
    private val teamName = view.findViewById<TextView>(R.id.team_name)
    private val teamItem = view.findViewById<LinearLayout>(R.id.container)

    fun bindItem(teams: Team, listener: (Team) -> Unit) {
        Picasso.with(itemView.context)
                .load(teams.teamBadge)
                .into(teamBadge)
        teamName.text = teams.teamName
        teamItem.onClick {
            listener(teams)
        }
    }
}