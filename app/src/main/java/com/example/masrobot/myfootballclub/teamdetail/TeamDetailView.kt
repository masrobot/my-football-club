package com.example.masrobot.myfootballclub.teamdetail

import com.example.masrobot.myfootballclub.model.Team

interface TeamDetailView {
    fun showLoading()
    fun hideLoading()
    fun showTeamDetail(data: List<Team>)
}